import { createStore } from 'vuex'

// Create a new store instance.
const store = createStore({
   state:{
       welcome_screen: true,
       team_screen: false,
       fixture_screen: false,
       week_screen:false,
       weeks:[],
       week:1,

      },
   mutations: {
      increment (state) {
         state.count++
      },
       addWeek (state, week) {
          state.weeks.push(week)
      }

   }
})

export default store;